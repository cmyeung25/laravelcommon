<?php
namespace Hbsz\LaravelCommon\Utility;

class TranslatorWrapper {
  private $engine;

  public function __construct($engine) {
    $this->engine = $engine;
  }

  public function setTranslatorEngine($engine){
    $this->engine = $engine;
    return $this;
  }

  public function setTargetLang($lang) {
    switch (get_class($this->engine)) {
      case "Stichoza\GoogleTranslate\TranslateClient":
        $this->engine->setTarget($lang);
        break;
      case "Dedicated\GoogleTranslate\Translator":
        $this->engine->setTargetLang($lang);
        break;
    }
  }

  public function translate($string) {
    return $this->engine->translate($string);
  }
}
