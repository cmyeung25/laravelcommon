<?php
namespace Hbsz\LaravelCommon\Utility\OnlineShopItem;

use Illuminate\Database\Eloquent\Model;
use Hbsz\LaravelCommon\Model\OnlineShopItem as OnlineShopItemModel;
use Sunra\PhpSimple\HtmlDomParser;

class OnlineShopItemShopDefault {
  public function getItem($url) {
    $shopItem = new OnlineShopItemModel();
      $raw = @file_get_contents($url);
      if($raw) {
        $dom = HtmlDomParser::str_get_html($raw);

        $shopItem->url = $url;

        $title = $dom->find("title");
        if(count($title) > 0) {
          $shopItem->title = $title[0]->plaintext;
        }

        foreach ($dom->find("meta") as $key => $meta) {
          if($meta->name == "description") {
            $shopItem->description = $meta->content;
          }
        }

        return $shopItem;
        // preg_match('/<meta/',$raw,$title);

        preg_match('/\{\"(.*)\;/',$raw,$json);
        $jsonArray = json_decode(substr($json[0],0,-1), true);
        //
        // $igUser = $jsonArray['entry_data']['ProfilePage'][0]['user'];
        // return [
        //   "id" => $igId,
        //   "username" => $igUser['full_name'],
        //   "posts" => $igUser['media']['count'],
        //   "followBy" => $igUser['followed_by']['count'],
        //   "profileImgUrl" => $igUser['profile_pic_url'],
        //   "isPrivate" => $igUser['is_private'],
        //   "medias" => $igUser['media']["nodes"],
        //   "mediasHasNext" => $igUser['media']["page_info"]["has_next_page"],
        // ];

        // preg_match('/\"followed_by\"\:\s?\{\"count\"\:\s?([0-9]+)/',$raw,$followBy);
        // preg_match('/\"profile_pic_url\"\: \"([a-zA-Z0-9\:\/\.\-\_]+)\"/',$raw,$profileUrl);
        // preg_match('/\"is_private\"\: ([a-zA-Z0-9\:\/\.\-\_]+)/',$raw,$isPrivate);
        //
        // preg_match('/\"media\"\:(.*)\"count\"\:\s?[0-9]+,/',$raw,$postsSource);
        // preg_match('/\"count\"\:(\s)?[0-9]+,/',$postsSource[0],$posts);
        //
        // dd($posts);
        // return [
        //   "username" => $igId,
        //   "posts" => intval($posts[0]),
        //   "followBy" => intval($followBy[1]),
        //   "profileImgUrl" => $profileUrl[1],
        //   "isPrivate" => $isPrivate[1],
        // ];
      } else {
        return [
          "error"=> "Invalid Url"
        ];
      }
  }


}
