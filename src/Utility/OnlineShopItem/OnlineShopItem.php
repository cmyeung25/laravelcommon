<?php
namespace Hbsz\LaravelCommon\Utility\OnlineShopItem;
use Hbsz\LaravelCommon\Model\OnlineShopItem as OnlineShopItemModel;
use Exception;
use ReflectionMethod;

class OnlineShopItem {
  protected $model = null;
  public function __construct($url) {
    $shop = $this->getOnlineShopByUrl($url);
    $this->initShopItemByUrl($shop, $url);
  }

  public function getModel() {
    return $this->model;
  }

  protected function getOnlineShopByUrl($url) {
    $shop = null;
    //url rules to test which factory to use
    // $shop = "AmazonJP";
    // $shop = "Default";
    if(strpos($url, 'taobao') !== false) {
      $shop = "Taobao";
    }

    if(strpos($url, 'm.tb.cn') !== false) {
      $shop = "TaobaoM";
    }

    if(strpos($url, 'tmall') !== false) {
      $shop = "Tmall";
    }

    // dd($shop);
    return $shop;
  }

  protected function initShopItemByUrl($shop, $url) {
    $className = "Hbsz\LaravelCommon\Utility\OnlineShopItem\OnlineShopItemShop". $shop;
    $class = new $className();
    $item = $class->getItem($url);

    $this->model = $item;
  }
}
