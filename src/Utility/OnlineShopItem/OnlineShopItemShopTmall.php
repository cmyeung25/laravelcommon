<?php
namespace Hbsz\LaravelCommon\Utility\OnlineShopItem;

use Illuminate\Database\Eloquent\Model;
use Hbsz\LaravelCommon\Model\OnlineShopItem as OnlineShopItemModel;
use Sunra\PhpSimple\HtmlDomParser;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;

class OnlineShopItemShopTmall {
  public function getItem($url) {
    $shopItem = new OnlineShopItemModel();
      // $raw = @file_get_contents($url);
      $shopItem->source = "tmall";
      $client = new Client(['allow_redirects' => ['max'=>200]]);

      $raw = $client->request('GET', $url);
      // dd($raw->getStatusCode());
      if($raw) {
        try {
          $raw = mb_convert_encoding($raw->getBody(),"utf-8","gb2312");
          $dom = HtmlDomParser::str_get_html($raw);

          $shopItem->url = $url;

          preg_match('/\((\s)*{\"(.*)\}/',$raw,$json);
          if(empty($json[0])) {
            throw new \Exception("Invalid Url");
          }
          $startPos = strpos($json[0],"{");

            // dd(strpos($json[0],"{"));
            $jsonArray = json_decode(substr($json[0],$startPos, strlen($json[0]) - $startPos), true);
            //https://detail.tmall.com/item.htm?id=561848034041&ali_refid=a3_430583_1006:1109825026:N:t%E6%81%A4%E7%94%B7:6d5015054117993ad86c4ce79dfd761e&ali_trackid=1_6d5015054117993ad86c4ce79dfd761e&spm=a230r.1.14.1

            // dd($jsonArray);

            $shopItem->title = $jsonArray['itemDO']['title'];
            $shopItem->description = $jsonArray['itemDO']['title'];
            $shopItem->currency = "CNY";

            $shopItem->images = [];
            $images = [];
            if(!empty($jsonArray['propertyPics'])){
              foreach($jsonArray['propertyPics']['default'] as $key => $pic) {
                $images[] = $pic;
              }
            } else {
              foreach ($dom->find("#J_UlThumb img") as $key => $image) {
                $images[] = str_replace("//","https://",str_replace("_60x60q90.jpg","",$image->src));
              }
            }
            $shopItem->images = $images;

            $shopItem->options = [];

            $options = [];

            $selectableOptions = [];

            $colorsImg = [];
            if(!empty($jsonArray['propertyPics'])) {
              foreach($jsonArray['propertyPics'] as $key => $propertyPic) {
                if($key != 'default' && !empty($propertyPic[0])) {
                  $colorsImg[str_replace(";","",$key)] = [
                    'img' => $propertyPic[0],
                    'key' => str_replace(";","",$key),
                  ];
                }
              }
            }

            $itemProps = [];
            foreach ($dom->find('.tm-sale-prop') as $key => $srcItemProp) {
              $itemProp = [];
              $itemProp['title'] = $srcItemProp->find("ul")[0]->getAttribute("data-property");
              $itemProp['options'] = [];
              foreach($srcItemProp->find('li') as $key1 => $srcListItem) {
                $option = [];
                $option['key'] = $srcListItem->getAttribute('data-value');
                $option['text'] = trim(str_replace("\t","",$srcListItem->plaintext));
                if(!empty($colorsImg[$option['key']])) {
                  $option['img'] = $colorsImg[$option['key']]['img'];
                }
                $itemProp['options'][] = $option;
              }
              $itemProps[] = $itemProp;
            }

            $shopItem->selectable_options = $itemProps;

            if(!empty($jsonArray['valItemInfo'])){
              foreach($jsonArray['valItemInfo']['skuList'] as $key => $srcOption) {
                $option = [];
                $option['name'] = $srcOption['names'];
                $option['pvs'] = $srcOption['pvs'];
                $option['skuId'] = $srcOption['skuId'];
                $options[] = $option;
              }

              foreach($options as &$option) {
                $key = ";".$option['pvs'].";";
                $option['price'] = 0;
                if(!empty($jsonArray['valItemInfo']['skuMap'][$key])) {
                  $mappedItem = $jsonArray['valItemInfo']['skuMap'][$key];
                  $option['price'] = $mappedItem['priceCent'] / 100;
                }
                $option['json'] = json_encode($option,JSON_UNESCAPED_UNICODE);
              }
            }

            $shopItem->options = $options;

            $shopItem->price = !empty($jsonArray['itemDO']['reservePrice']) ? (float) $jsonArray['itemDO']['reservePrice'] : '';
            // dd($shopItem);
            $shopItem->html_content = $dom->find("#attributes",0)->outertext;
            return $shopItem;

        } catch (\Exception $e) {
          return [
            "error"=> "Invalid Url"
          ];
        }


        // $title = $dom->find("title");
        // if(count($title) > 0) {
        //   $shopItem->title = $title[0]->plaintext;
        // }

        //title
        //description
        // foreach ($dom->find("meta") as $key => $meta) {
        //   if($meta->name == "keywords") {
        //     $shopItem->title = $meta->content;
        //   }
        //   if($meta->name == "description") {
        //     $shopItem->description = $meta->content;
        //   }
        // }
        // $shopItem->price = $dom->find()
        // //image
        // $images = [];
        // foreach ($dom->find("#J_UlThumb img") as $key => $image) {
        //   $images[] = str_replace("//","https://",str_replace("_60x60q90.jpg","",$image->src));
        // }
        //
        // //properies
        // $itemProps = [];

        // $shopItem->itemProps = $itemProps;
        // $shopItem->images = $images;

      } else {
        return [
          "error"=> "Invalid Url"
        ];
      }
  }
}
