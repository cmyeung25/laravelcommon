<?php
namespace Hbsz\LaravelCommon\Utility\OnlineShopItem;

use Illuminate\Database\Eloquent\Model;
use Hbsz\LaravelCommon\Model\OnlineShopItem as OnlineShopItemModel;
use Sunra\PhpSimple\HtmlDomParser;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;

class OnlineShopItemShopTaobao {
  public function getItem($url) {
    $shopItem = new OnlineShopItemModel();
      // $raw = @file_get_contents($url);
      $shopItem->source = "taobao";

      $client = new Client(['allow_redirects' => ['max'=>200]]);

      $raw = $client->request('GET', $url);

      // dd($raw->getStatusCode());
      if($raw) {
        $raw = mb_convert_encoding($raw->getBody(),"utf-8","gb2312");
        $dom = HtmlDomParser::str_get_html($raw);

        $shopItem->url = $url;

        preg_match('/Hub\.config\.set\(\'sku\'\,(.*\s)*/',$raw,$json);
        $json = str_replace("\n","",substr($json[0],strpos($json[0],"{"), strpos($json[0],"});") - strpos($json[0],"{") + 1));
          $json = str_replace(" valCartInfo","\"valCartInfo\" ",$json);
          $json = str_replace(" itemId","\"itemId\" ",$json);
          $json = str_replace(" cartUrl","\"cartUrl\" ",$json);
          $json = str_replace(" apiRelateMarket","\"apiRelateMarket\" ",$json);
          $json = str_replace(" apiAddCart","\"apiAddCart\" ",$json);
          $json = str_replace(" apiInsurance","\"apiInsurance\" ",$json);
          $json = str_replace(" wholeSibUrl","\"wholeSibUrl\" ",$json);
          $json = str_replace(" areaLimit","\"areaLimit\" ",$json);
          $json = str_replace(" bigGroupUrl","\"bigGroupUrl\" ",$json);
          $json = str_replace(" valPostFee","\"valPostFee\" ",$json);
          $json = str_replace(" couponApi","\"couponApi\" ",$json);
          $json = str_replace(" couponWidgetDomain","\"couponWidgetDomain\" ",$json);
          $json = str_replace(" coupon","\"coupon\" ",$json);
          $json = str_replace(" cbUrl","\"cbUrl\" ",$json);
          $json = str_replace(" valItemInfo","\"valItemInfo\" ",$json);
          $json = str_replace(" defSelected","\"defSelected\" ",$json);
          $json = str_replace(" skuMap","\"skuMap\" ",$json);
          $json = str_replace("propertyMemoMap","\"propertyMemoMap\" ",$json);
          $json = str_replace("'","\"",$json);
        // dd($json);
        $jsonArray = json_decode($json, true);

        $itemId = $jsonArray['valCartInfo']['itemId'];
          // dd(str_replace("\n","",substr($json[0],strpos($json[0],"{"), strpos($json[0],"});") - strpos($json[0],"{") + 1)));
        $dataQuery = urlencode('{"exParams":"itemNumId":"' . $itemId. '"}');
        $queryUrl = "https://h5api.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?jsv=2.4.4&appKey=12574478&t=1515652882840&sign=0921e12ab77aa30cdd1de26e4a4cdd02&api=mtop.taobao.detail.getdetail&v=6.0&ttid=2017@htao_h5_1.0.0&type=json&dataType=json&data={$dataQuery}";

        $apiResult = $client->request('GET', $queryUrl);

        if($apiResult) {
          $apiJson = json_decode($apiResult->getBody(), true);
        }
        //https://h5api.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?jsv=2.4.4&appKey=12574478&t=1515652882840&sign=0921e12ab77aa30cdd1de26e4a4cdd02&api=mtop.taobao.detail.getdetail&v=6.0&ttid=2017%40htao_h5_1.0.0&type=jsonp&dataType=jsonp&callback=mtopjsonp1&data=%7B%22exParams%22%3A%22%7B%5C%22countryCode%5C%22%3A%5C%22HK%5C%22%7D%22%2C%22itemNumId%22%3A%2245666707702%22%7D


        // dd($jsonArray);

        $shopItem->title = $apiJson['data']['item']['title'];
        $shopItem->description = $apiJson['data']['item']['title'];
        $shopItem->currency = "CNY";

        $shopItem->images = [];
        $shopItem->images = $apiJson['data']['item']['images'];

        $itemProps = [];
        if(!empty($apiJson['data']['skuBase']['props'])) {
          foreach ($apiJson['data']['skuBase']['props'] as $key => $props) {
            $itemProp = [];
            $itemProp['title'] = $props['name'];
            $itemProp['options'] = [];
            foreach ($props['values'] as $key => $value) {
              $option = [];
              $option['key'] = $props['pid'].":".$value['vid'];
              $option['text'] = $value['name'];
              if(!empty($value['image'])) {
                $option['img'] = $value['image'];
              }
              $itemProp['options'][$option['key']] = $option;
            }
            $itemProps[] = $itemProp;
          }
        }
        $shopItem->selectable_options = $itemProps;

        if(!empty($apiJson['data']['skuBase']['skus'])) {
          $skusPropPathMappingList = $apiJson['data']['skuBase']['skus'];
          $skus2PropList = [];
          foreach ($skusPropPathMappingList as $key => $value) {
            $propPaths = explode(";", $value['propPath']);
            $selectedProps = "";
            foreach ($propPaths as $key => $path) {
              $selectedProps = $selectedProps . " " . $shopItem->selectable_options[$key]['options'][$path]['text'];
            }
            $value['description'] = $selectedProps;
            $skus2PropList[$value['skuId']] = $value;
          }

          $options = [];
          foreach ($apiJson['data']['apiStack'] as $key => $apiStack) {
            if($apiStack['name'] == "esi") {
              $esiData = json_decode($apiJson['data']['apiStack'][$key]['value'],true);
              // dd($esiData);
              $option = [];
              foreach ($esiData['skuCore']['sku2info'] as $key => $value) {
                $option['price'] = $value['price']['priceMoney'] / 100;
                if(!empty($skus2PropList[$key])){
                  $option['name'] = $skus2PropList[$key]['description'];
                  $option['pvs'] = $skus2PropList[$key]['propPath'];
                  $option['json'] = json_encode($skus2PropList[$key]);
                } else {
                  $option['name'] = "";
                  $option['pvs'] = "";
                  $option['json'] = "";
                }
                $options[] = $option;
              }
            }
          }
          $shopItem->options = $options;
        } else {
          foreach ($apiJson['data']['apiStack'] as $key => $apiStack) {
            if($apiStack['name'] == "esi") {
              $esiData = json_decode($apiJson['data']['apiStack'][$key]['value'],true);
              // dd($esiData);
              $option = [];
              foreach ($esiData['skuCore']['sku2info'] as $key => $value) {
                $option['price'] = $value['price']['priceMoney'] / 100;
                $option['name'] = "";
                $option['pvs'] = "";
                $option['json'] = "";
                $options[] = $option;
              }
            }
          }
          $shopItem->options = $options;
        }

        $shopItem->html_content = $dom->find("#attributes",0)->outertext;

        // $shopItem->selectable_options = $itemProps;
        // $shopItem->options = $options;
        // $shopItem->description = $jsonArray['itemDO']['title'];
        // $shopItem->images = [];


        return $shopItem;
      } else {
        return [
          "error"=> "Invalid Url"
        ];
      }
  }
}
