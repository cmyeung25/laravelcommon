<?php
namespace Hbsz\LaravelCommon\Utility\Notification;

Interface NotificationTypeInterface {
    public static function send($view, array $data, $callback);
}
