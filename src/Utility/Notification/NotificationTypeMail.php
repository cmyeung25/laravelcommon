<?php
namespace Hbsz\LaravelCommon\Utility\Notification;

use Mail;
use Illuminate\Database\Eloquent\Model;

class NotificationTypeMail implements NotificationTypeInterface {
    public static function initNotification(Model $model, $mail) {
        $swiftMessage = $mail->getSwiftMessage();
        $model->to = $swiftMessage->getTo();
        $model->bcc = $swiftMessage->getBcc();
        $model->cc = $swiftMessage->getCc();
        $model->from = $swiftMessage->getFrom();
        $model->subject = $swiftMessage->getSubject();
        $model->content = $swiftMessage->getBody();
        return $model;
    }

    public static function send($view, array $data, $callback){
        return Mail::send($view, $data, $callback);
    }

}

// $mail->send('emails.emailTest', ['user' => $user ], function($m) use ($user) {
//     $m->subject("Testing Mail");
//     $m->to($user->email, $user->name);
// });
