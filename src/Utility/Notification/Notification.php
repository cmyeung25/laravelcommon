<?php
namespace Hbsz\LaravelCommon\Utility\Notification;
use Hbsz\LaravelCommon\Model\Notification as NotifactionModel;
use Exception;

class Notification {
    protected $type = '';
    protected $notifactionTypeClass = "";

    protected function getNotificationClass($type) {
        $fqcn = null;
        switch ($type) {
            case 'mail':
                $fqcn = NotificationTypeMail::class;
                break;
            case 'sms':
                $fqcn = NotificationTypeSms::class;
                break;
            default:
                $fqcn = null;
                break;
        }

        if($fqcn == null) {
            throw new Exception("Cannot find the notification class for type: {$type}", 1);
        }
        return $fqcn;
    }


    public function __construct($type = 'mail') {
        $this->type = $type;
        $this->notifactionTypeClass = $this->getNotificationClass($type);
    }

    public function send($view, array $data, $callback){
        $model = new NotifactionModel();
        $class = new $this->notifactionTypeClass;

        $result = $class->send($view, $data, function($mail) use ($callback, $model, $class){
            $callback($mail);
            $model = $class->initNotification($model, $mail);
            $model->save();
        });

        $model->status = 'completed';
        $model->save();

        return $result;
    }






}
