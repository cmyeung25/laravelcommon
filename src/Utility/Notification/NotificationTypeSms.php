<?php
namespace Hbsz\LaravelCommon\Utility\Notification;

use Illuminate\Database\Eloquent\Model;
use Config;
use Exception;
use App;

class NotificationTypeSms implements NotificationTypeInterface {
    public static function initNotification(Model $model, $mail) {
        $model->to = $mail->getTo();
        $model->content = $mail->getBody();
        return $model;
    }

    public static function send($view, array $data, $callback){
      $sms = new Sms();
      return $sms->send($view, $data, $callback);
    }
}

class Sms {
  protected $to;
  protected $message;
  protected $pwd;
  protected $accountno;
  protected $from;
  function __construct() {
    $this->pwd = Config::get('notification.sms.pwd');
    $this->accountno = Config::get('notification.sms.accountno');
    if(empty($this->pwd) || empty($this->accountno)) {
      throw new Exception("Missing configuration on sms account", 1);
    }
  }

  public function send($view, $data, $callback) {
    $this->message = view($view,$data)->render();
    $callback($this);


    if(App::environment() == 'production') {
      $pwd = $this->pwd;
      $accountno = $this->accountno;
      $msg = urlencode($this->message);
      $phone = $this->getTo();
      $from = $this->getFrom();
      // $url = "http://api.accessyou.com/sms/sendsms-utf8-senderid.php?phone=$phone&pwd=$pwd&accountno=$accountno&from=$from&dnc=y&size=l&msg=$msg";//senderID not support
      $url = "http://api.accessyou.com/sms/sendsms-utf8.php?phone=$phone&pwd=$pwd&accountno=$accountno&size=l&msg=$msg";
      $handle = fopen($url, "r");
      $contents = trim(fread($handle, 8192));
    } else {
      $phone = $this->getTo();
      echo "DEVELOPMENT:\n";
      echo "SMS CONTENT: {$this->message}\n";
      echo "RECIPTENT: {$phone}\n";
    }
  }
  public function to($value) {
    $this->to = $value;
  }
  public function getTo() {
    if(!empty($this->to)) {
      if($this->validPhone($this->to)) {
        return '852'.$this->to;
      } else {
        throw new Exception("Please enter the correct phone format. Currenly support 8 digits Hong Kong Phone Only.", 1);
      }
    } else {
        throw new Exception("It is not allow to send sms without recipient", 1);
    }
  }

  public function getBody() {
    return $this->message;
  }

  public function from($value) {
    $this->from = $value;
  }
  public function getFrom() {
    return $this->from;
  }

  protected function validPhone($value) {
    if(!is_numeric($value)) {
      return false;
    }

    if(strlen($value) != 8) {
      return false;
    }

    return true;
  }
}



// $message = "出示此短訊於12月31日前親臨{{store_name}}地址{{store_address}}即可獲贈柔滑美肌粉底霜試用裝乙件。立即感受光澤細緻肌! 換領編號: {{sample_code}} 此短訊不能轉發。查詢EN/取消UN{{store_phone}}";
// $message = $campaign->locale->campaign_sms_message;
// $msg = $this->smsMessageMerge($message, $sample);
//
// $msg = urlencode($this->smsMessageMerge($message, $sample));
// $phone = strlen($sample->phone) <= 8 ? '852'.$sample->phone : $sample->phone;
// $pwd = "63323172";
// $accountno = "11028379";
// $url = "http://api.accessyou.com/sms/sendsms-utf8-senderid.php?phone=$phone&pwd=$pwd&accountno=$accountno&from=COVERMARKHK&dnc=y&size=l&msg=$msg";
//
// $handle = fopen($url, "r");
// $contents = trim(fread($handle, 8192));
