<?php
namespace Hbsz\LaravelCommon\Utility;
use Config;
use LaravelLocalization;

class Helper {
    public static function buildUrlQuery($url, array $query)
    {
        // append the query string
        if (!empty($query)) {
            $queryString = http_build_query($query);
            $url .= '?' . $queryString;
        }

        return $url;
    }

    public static function langUrl($url)
    {
        return LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), url($url));
    }

    public static function getBackendPrefix() {
        return Config::get('backend.url');
    }

    public static function backendUrl($uri = '/')
    {
        return url(Config::get('backend.url') . '/' . $uri);
    }

    public static function frontendImageUrl($uri, $locale = false )
    {
        if($locale == false)
            return asset("themes/frontend/img/{$uri}");
        else
            return asset("themes/frontend/img/" . LaravelLocalization::getCurrentLocale() . "/{$uri}");
    }
}
