<?php
/**
 * Fix for 7.0.0 extension
 */

namespace Hbsz\LaravelCommon\Utility\BladeExtensions;

use Illuminate\Contracts\Container\Container;
use Radic\BladeExtensions\Contracts\HelperRepository;
use Radic\BladeExtensions\Directives\AbstractDirective;
/**
 * This is the class EmbedDirective.
 *
 * @author  Robin Radic
 */
class EmbedDirective extends AbstractDirective
{
    protected $pattern = '/(?<!\w)(\s*)@NAME\s*\((.*?)\)\s*$((?>(?!@(?:end)?NAME).|(?0))*)@endNAME/sm';

    protected $replace = <<<'EOT'
$1<?php app('blade-extensions.helpers')->get('embed')->start($2); ?>
$1<?php app('blade-extensions.helpers')->get('embed')->current()->setData(\$__data)->setContent(<<<'EOT_'
$3
EOT_
); ?>
$1<?php app('blade-extensions.helpers')->get('embed')->end(); ?>
EOT;

    /**
     * DumpDirective constructor.
     */
    public function __construct(HelperRepository $helpers, Container $container)
    {

        $helpers->put('embed', $container->make(EmbedHelper::class));
    }
}
