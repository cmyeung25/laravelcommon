@set('class', isset($class) ? $class : 'paginator')
<nav class="{{ $class }}">
    <span>{{ $paginator['from'] }} - {{ $paginator['to'] }} of {{ $paginator['total'] }}</span>
    <ul>
        <li>
            <a href="{!! $paginator['first_page_url'] !!}"><i class="fa fa-step-backward"></i></a>
        </li>
        <li>
            <a href="{!! $paginator['prev_page_url'] !!}"><i class="fa fa-chevron-left"></i></a>
        </li>
        <li>
            <a href="{!! $paginator['next_page_url'] !!}"><i class="fa fa-chevron-right"></i></a>
        </li>
        <li>
            <a href="{!! $paginator['last_page_url'] !!}"><i class="fa fa-step-forward"></i></a>
        </li>
    </ul>
</nav>
