{{--
$field_class
$field_value
$field_label
$field_name
--}}

@set('field_class', isset($field_class) ? $field_class : '')
@set('field_value', isset($field_value) ? $field_value : '')
@set('field_icon_backgound_color', isset($field_icon_backgound_color) ? $field_icon_backgound_color : '')
@set('field_attr', isset($field_attr) ? $field_attr : [])

<div class="form-group checkbox {{ $field_class }}">
    <label>
        <input type="checkbox" value="{{ $field_value }}" name="{{ $field_name }}"
        @foreach($field_attr as $key => $value)
            {{$key}}={{$value}}
        @endforeach
        >
        <span class="{{ $field_icon_backgound_color }}">{!! $field_label !!}</span>
    </label>
    @if( !empty($field_hidden_value) )
        <input type="hidden" name="{{ $field_name }}" value="{{ $field_hidden_value }}">
    @endif
    <span class="error-message"></span>
</div>
