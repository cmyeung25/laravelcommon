@set('field_class', isset($field_class) ? $field_class : '')
@set('field_zoom', isset($field_zoom) ? $field_zoom : 8)
@set('field_lat_value', !empty($field_lat_value) ? $field_lat_value : '22.3203834')
@set('field_lng_value', !empty($field_lng_value) ? $field_lng_value : '114.1697931')
@set('field_autocomplete', !empty($field_autocomplete) ? true : false )
@set('field_api_include', !empty($field_api_include) ? true : false )
@set('field_height', !empty($field_height) ? $field_height : false )

<div class="form-group map-picker {{ $field_class }}">
    <label>{{ $field_label }}</label>
    @if (!empty($field_autocomplete))
        <input type="text" id="map_{{ $field_id }}_autocomplete" value="">
    @endif
    <div class='map-latlng-picker' style="{{ !empty($field_height) ? 'height:'.$field_height : '' }}" id='map_{{ $field_id }}'></div>
    <script type='text/javascript'>
    var map_{{ $field_id }};
    var marker_{{ $field_id }};
    function initMap_{{ $field_id }}() {
        map_{{ $field_id }} = new google.maps.Map(document.getElementById('map_{{ $field_id }}'), {
            @if (!empty($field_lat_value))
                center: {lat: {{ $field_lat_value }}, lng: {{ $field_lng_value }} },
            @else
                center: {lat: -34.397, lng: 150.644},
            @endif
            zoom: {{ $field_zoom }}
        });
        marker_{{ $field_id }} = new google.maps.Marker({
            position: {lat: {{ $field_lat_value }}, lng: {{ $field_lng_value }} },
            map: map_{{ $field_id }},
            draggable: true,
        });
        marker_{{ $field_id }}.addListener('dragend', function(marker_{{ $field_id }}){
            $('#map_{{ $field_id }}_lat').val(marker_{{ $field_id }}.latLng.lat());
            $('#map_{{ $field_id }}_lng').val(marker_{{ $field_id }}.latLng.lng());
        })

        @if (!empty($field_autocomplete))
            // var input = $('#map_{{ $field_id }}_autocomplete');
            var input = document.getElementById('map_{{ $field_id }}_autocomplete');

            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map_{{ $field_id }});

            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                marker_{{ $field_id }}.setPosition(place.geometry.location);
                map_{{ $field_id }}.setCenter(place.geometry.location);
                $('#map_{{ $field_id }}_lat').val(place.geometry.location.lat());
                $('#map_{{ $field_id }}_lng').val(place.geometry.location.lng());

                if (typeof(map_{{ $field_id }}_place_changed_function) == 'function') {
                    map_{{ $field_id }}_place_changed_function(place);

                }
            });
        @endif

    }

    </script>
    @if($field_api_include)
        <script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap_{{ $field_id }}&libraries=places"></script>
    @endif

    <input type="hidden" id="map_{{ $field_id }}_lat" name="{{ $field_lat_name }}" value="{{ $field_lat_value }}">
    <input type="hidden" id="map_{{ $field_id }}_lng" name="{{ $field_lng_name }}" value="{{ $field_lng_value }}">
</div>
