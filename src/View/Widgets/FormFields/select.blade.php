@set('field_class', isset($field_class) ? $field_class : '')
@set('field_multiple', isset($field_multiple) ? $field_multiple : false)
@set('field_istag', isset($field_istag) ? $field_istag : false)
@set('field_placeholder', isset($field_placeholder) ? $field_placeholder : 'Please select..')
@set('field_value', ($field_multiple and !isset($field_value)) ? [] : $field_value)
@set('field_attr', isset($field_attr) ? $field_attr : [])
@set('field_options', isset($field_options)? $field_options : [])
@set('field_allow_empty', isset($field_allow_empty)? $field_allow_empty : true)
@set('field_basic', isset($field_basic)? $field_basic : false)
    <div class="form-group select {{ $field_class }}">
        <label for="{{ $field_id }}">{{ $field_label }}</label>
            <select class="form-control @if(!$field_basic) {{ $field_istag ? 'select2-tag' : 'select2' }} @endif" name="{{ $field_name }}" id="{{ $field_id }}" {{ $field_multiple ? 'multiple' : '' }}
                @foreach($field_attr as $key => $value)
                    {{$key}}={{$value}}
                @endforeach
            >
                @if($field_allow_empty)
                    <option value="">{{ $field_placeholder }}</option>
                @endif
                @foreach($field_options as $key => $value)
                    @if(is_array($value) and !empty($value['optgroup']) and $value['optgroup'])
                        <optgroup label="{{ $value['display'] }}">
                            @if(!empty($value['children']))
                                @foreach($value['children'] as $key => $value)
                                    <option
                                        @if($field_multiple)
                                            @if( in_array($key, $field_value )) selected @endif
                                        @else
                                            @if( $field_value == $key ) selected @endif
                                        @endif

                                        @if(is_array($value))
                                            @foreach( $value as $attributeKey => $attributeValue )
                                                data-{{$attributeKey}}="{{$attributeValue}}"
                                            @endforeach
                                        @endif
                                    value="{{ $key }}">
                                        @if(is_array($value))
                                            {{ $value['display'] }}
                                        @else
                                            {{ $value }}
                                        @endif
                                    </option>
                                @endforeach
                            @endif
                        </optgroup>
                    @else
                        <option
                            @if($field_multiple)
                                @if( in_array($key, $field_value )) selected @endif
                            @else
                                @if( $field_value == $key ) selected @endif
                            @endif

                            @if(is_array($value))
                                @foreach( $value as $attributeKey => $attributeValue )
                                    data-{{$attributeKey}}="{{$attributeValue}}"
                                @endforeach
                            @endif
                        value="{{ $key }}">
                            @if(is_array($value))
                                {{ $value['display'] }}
                            @else
                                {{ $value }}
                            @endif
                        </option>
                    @endif
                @endforeach
            </select>
            <span class="error-message"></span>
    </div>
