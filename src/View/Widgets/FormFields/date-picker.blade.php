@set('field_class', isset($field_class) ? $field_class : '')
@set('field_value', isset($field_value) ? $field_value : '')
@set('field_attr', isset($field_attr) ? $field_attr : [])
@set('field_name', isset($field_name) ? $field_name : '')
@set('field_init', isset($field_init) ? $field_init : true)
@set('field_js_format', isset($field_js_format) ? $field_js_format : 'YYYY-MM-DD')

<div class="form-group datetimepicker {{ $field_class }}">
    <label>{{ $field_label }}</label>
    <div class='input-group date' id='datepicker-{{ $field_id }}'>
        <input type='text' class="form-control" name="{{ $field_name }}" value="{{ $field_value }}">
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
</div>

@if($field_init)
    <script type='text/javascript'>
      document.addEventListener("DOMContentLoaded", function(event) {
        var datePicker_{{ $field_id }} = $('#datepicker-{{ $field_id }}').datetimepicker({
          format: '{{ $field_js_format }}'
        });
      })
    </script>
@endif
