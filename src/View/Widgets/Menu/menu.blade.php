@set('level', isset($level) ? $level : 1)
@set('class', isset($class) ? $class : '')
<ul class="{{ $class }}">
    @foreach($menuItem as $item)
        <li class="{{ $item['class']  }} {{ $item['isActive']  ? 'active' : '' }}">
            @if (!empty($item['realPath']))
            <a href="{{ $item['realPath'] }}">
            @endif
                <span>{!! $item['label']  !!}</span>
                @if(!empty($item['children'] ))
                    @embed('Widgets.Menu.menu', ['menuItem' => $item['children'], 'level' => $level + 1 ])
                    @endembed
                @endif
            @if (!empty($item['realPath']))
            </a>
            @endif
        </li>
    @endforeach
</ul>
