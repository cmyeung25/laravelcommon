@set('keyword', isset($keyword) ? $keyword : '')
<div class="row">
    <div class="col-md-12">
        @embed('Widgets.Layout.box')
            @section('box-title' , 'Data Table')
            @section('box-tools')
                <form class="search-form" action="{{ $search_url }}" method="get">
                    <label for="search-box" class="search-box-label"><i class="fa fa-search"></i></label>
                    <input class="search-box" type="text" name="keyword" id="search-box" value="{{ $keyword }}">
                </form>
                <a href="{{ $add_new_url }}"><i class="fa fa-plus-square"></i> Add New Record</a>
            @endsection
            @section('box-body')
                @embed('Widgets.DataTable.data-table', ['dataTable' => $dataTable])
                @endembed
                @embed('Widgets.Paginator.paginator-simple', ['paginator' => $paginator, 'class' => 'data-table-paginator'])
                @endembed
            @endsection
        @endembed

    </div>
</div>
