<?php

namespace Hbsz\LaravelCommon\Model;

use Illuminate\Database\Eloquent\Model;
use Stichoza\GoogleTranslate\TranslateClient;
use Sunra\PhpSimple\HtmlDomParser;

class OnlineShopItem extends Model {
  protected $fillable = [
    'source',
    'url',
    'title',
    'description',
    'currency',
    'images',
    'options',
    'selectable_options',
    'html_content',
    'price', //only available if options is not available
  ];

  //url
  //title
  //description
  //currency
  //images
  //options
    //[]
  //selectable_options
    //[key=>[values]]


  //weight
  //height
  //width
  //depth
  public function translate($langaug, $tr) {
    $this->title = $tr->translate($this->title);
    $this->description = $tr->translate($this->description);

    $dom = HtmlDomParser::str_get_html($this->html_content);
    $translatedDoms = [];
    $translatedText = "";
    foreach ($dom->nodes as $key => &$node) {
      if(!empty(trim($node->innertext)) && empty($node->children) && $node->tag == 'text') {
        $node->innertext = $tr->translate(html_entity_decode($node->innertext));
        $translatedDoms[] = $node;
        $translatedText .=  $node->innertext . "<br />";
      }
    }
    $this->html_content = $translatedText;

    $options = [];
    foreach($this->options as $option) {
      $option['name'] = $tr->translate($option['name']);
      $options[] = $option;
    }
    $this->options = $options;

    $selectable_options = [];
    foreach($this->selectable_options as $selectable_option) {
      $selectable_option['title'] = $tr->translate($selectable_option['title']);
      $options = [];
      foreach ($selectable_option['options'] as $key => $option) {
        $option['text'] = $tr->translate($option['text']);
        $options[] = $option;
      }
      $selectable_option['options'] = $options;
      $selectable_options[] = $selectable_option;
    }
    $this->selectable_options = $selectable_options;

    // description
    // options
    //   name
    // selectable_options
    //   title
    //   options
    //     text
    // html_content
  }
}
