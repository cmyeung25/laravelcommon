<?php

namespace Hbsz\LaravelCommon\Model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {
    protected $casts = [
        'to' => 'array',
        'cc' => 'array',
        'bcc' => 'array',
        'from' => 'array',
    ];
}
