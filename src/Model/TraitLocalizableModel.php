<?php

namespace Hbsz\LaravelCommon\Model;

use Illuminate\Database\Eloquent\Builder;
use LaravelLocalization;
use Closure;
use DB;

trait TraitLocalizableModel
{
    public function i18ns()
    {
        return $this->hasMany(__CLASS__.'I18n', $this->getI18nForeignKey());
    }

    public function scopeWithLocale(Builder $builder, $locale)
    {
        return $builder
                ->joinI18nWithLocale($locale)
                ->with(['i18n' => function ($query) use ($locale) {
                    $query->where('locale', $locale);
                }]);
    }

    public function getI18nClass()
    {
        return __CLASS__.'I18n';
    }

    public function i18n()
    {
        return $this->hasOne(__CLASS__.'I18n', $this->getI18nForeignKey());
    }

    /**
     * For search function.
     */
    public function scopeJoinI18n(Builder $builder)
    {
        $i18nClassName = __CLASS__.'I18n';
        $i18nInstance = new $i18nClassName();

        return $builder->leftJoin($i18nInstance->getTable(), $this->getTable().'.'.$this->primaryKey, '=', $i18nInstance->getTable().'.'.$this->getI18nForeignKey());
    }

    public function scopeJoinI18nWithLocale(Builder $builder, $locale)
    {
        $i18nClassName = __CLASS__.'I18n';
        $i18nInstance = new $i18nClassName();

        return $builder->leftJoin($i18nInstance->getTable(), function ($join) use ($i18nInstance, $locale) {
            $join
            ->on($this->getTable().'.'.$this->primaryKey, '=', $i18nInstance->getTable().'.'.$this->getI18nForeignKey())
            ->where('locale', '=',  $locale);
        })->select('*')
        ->addSelect($this->getTable().'.'.$this->primaryKey)
        ;
    }

    /**
     * @param $wheres array Array for where clause
     */
    public static function searchWithI18n(Closure $callback)
    {
        $instance = new static();

        return $instance->whereIn($instance->getTable().'.'.$instance->primaryKey, function ($query) use ($instance, $callback) {
            $i18nClassName = __CLASS__.'I18n';
            $i18nInstance = new $i18nClassName();
            $query->from($instance->getTable());
            $query->join($i18nInstance->getTable(), $instance->getTable().'.'.$instance->primaryKey, '=', $i18nInstance->getTable().'.'.$this->getI18nForeignKey());
            $query->select($instance->getTable().'.'.$instance->primaryKey);
            $callback($query);
        });
    }

    /**
     * By default all i18n content will be generated;.
     */
    public static function create(array $attributes = array())
    {
        $model = parent::create($attributes);
        $i18nClassName = __CLASS__.'I18n';

        foreach (LaravelLocalization::getSupportedLocales() as $key => $locale) {
            $attributes = array_merge($attributes, [
                                $model->getI18nForeignKey() => $model[$model->primaryKey],
                                'locale' => $key,
                            ]);
            $modelI18n = $i18nClassName::create($attributes);
        }

        // $model->i18n = $modelI18n;
        return $model;
    }

    public static function createWithMultiLocales(array $attributes = array())
    {
        $model = parent::create($attributes);
        $i18nClassName = __CLASS__.'I18n';

        foreach ($attributes['i18ns'] as $key => $i18n) {
            $i18n = array_merge($i18n, [
                $model->getI18nForeignKey() => $model[$model->primaryKey],
                'locale' => $key,
            ]);
            $modelI18n = $i18nClassName::create($i18n);
        }

        return $model;
    }

    public function update(array $attributes = array())
    {
        $singleAttributes = $attributes;
        unset($singleAttributes['i18ns']);
        unset($singleAttributes['sync']);

        $result = parent::update(array_merge([
                $this->getTable().'_updated_at' => DB::raw('now()'),
            ], $singleAttributes));

        $primaryKey = $this[$this->primaryKey];
        $model = $this->withLocale($singleAttributes['locale'])->find($primaryKey);

        if (empty($model->i18n)) {
            $i18nClassName = __CLASS__.'I18n';
            $singleAttributes = array_merge($singleAttributes, [
                                $this->getI18nForeignKey() => $primaryKey,
                            ]);
            $modelI18n = $i18nClassName::create($singleAttributes);
            $model->i18n = $modelI18n;
        }

        $model->i18n->update($singleAttributes);

        if (!empty($attributes['i18ns']) or !empty($attributes['sync'])) {
            return $this->updateWithMultiLocales($attributes);
        }

        return $result;
    }

    public function updateWithMultiLocales(array $attributes = array())
    {
        $result = parent::update(array_merge([
                $this->getTable().'_updated_at' => DB::raw('now()'),
            ], $attributes));

        $primaryKey = $this[$this->primaryKey];

        if (!empty($attributes['i18ns'])) {
            foreach ($attributes['i18ns'] as $key => $i18n) {
                $model = $this->withLocale($key)->find($primaryKey);
                if (empty($model->i18n)) {
                    $i18nClassName = __CLASS__.'I18n';
                    $i18n = array_merge($i18n, [
                                        $this->getI18nForeignKey() => $primaryKey,
                                    ]);
                    $modelI18n = $i18nClassName::create($i18n);
                    $model->i18n = $modelI18n;
                }
                $model->i18n->update($i18n);
            }
        }

        if (!empty($attributes['sync'])) {
            foreach (LaravelLocalization::getSupportedLocales() as $key => $value) {
                $model = $this->withLocale($key)->find($primaryKey);
                if (empty($model->i18n)) {
                    $i18nClassName = __CLASS__.'I18n';
                    $i18n = array_merge($i18n, [
                                        $this->getI18nForeignKey() => $primaryKey,
                                    ]);
                    $modelI18n = $i18nClassName::create($i18n);
                    $model->i18n = $modelI18n;
                }
                $model->i18n->update($attributes['sync']);
            }
        }

        return $result;
    }

    abstract protected function getI18nForeignKey();
}
