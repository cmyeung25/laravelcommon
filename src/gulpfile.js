var elixir = require('laravel-elixir');
var bourbon = require('bourbon');
var gulp = require('gulp');
var livereload = require('gulp-livereload');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


elixir(function(mix) {

    mix.sass('backend.scss', 'export/css/backend.css', {
        includePaths: bourbon,
        outputStyle: 'compact'
    });

});
