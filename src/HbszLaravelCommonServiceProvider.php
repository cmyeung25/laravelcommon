<?php

namespace Hbsz\LaravelCommon;

use Illuminate\Support\ServiceProvider;
use Radic\BladeExtensions\BladeExtensionsServiceProvider;
use Config;
use Gate;
use Radic\BladeExtensions\HelperRepository;

class HbszLaravelCommonServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        $this->app->register(BladeExtensionsServiceProvider::class);
        $this->publishes([
            __DIR__.'/config/backend.php' => config_path('backend.php'),
            __DIR__.'/config/login.php' => config_path('login.php'),
            __DIR__.'/config/notification.php' => config_path('notification.php'),
            __DIR__.'/database/migrations/' => database_path('migrations'),
        ], 'config');
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        //config file for backend
        $packageConfigFile = __DIR__.'/config/backend.php';
        $this->mergeConfigFrom(
            $packageConfigFile, 'backend'
        );

        //config file for views
        $viewPaths = (Config::get('view.paths'));
        Config::set('view.paths', array_merge($viewPaths, [
            __DIR__.'/View',
        ]));
        $this->registerUserGate();
        $this->registerOverrideBladeExtension();
    }

    public function registerUserGate()
    {
        Gate::define('backendAccess', function ($user) {
            return $user->type == 'admin';
        });
    }

    public function registerOverrideBladeExtension() {
      $this->app->singleton('blade-extensions.helpers', function ($app) {
          $helpers = new HelperRepository($app);
          $helpers->register('embed', \Hbsz\LaravelCommon\Utility\BladeExtensions\EmbedHelper::class);
          return $helpers;
      });
    }
}
