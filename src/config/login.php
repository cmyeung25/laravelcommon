<?php

return [
    'backendAuth' => [
        'loginTemplate' => 'backend.auth.login',
        'redirectPath' => 'backend/dashboard',
        'redirectAfterLogout' => 'backend/auth/login',
        'logoutPath' => 'backend/auth/logout',

        'registerTemplate' => 'backend.auth.register',
        'registerDirectLogin' => false, // define if required user activation flow after registerTemplate (ture: direct login | false: email with link to activate required)

        'registerActivationEmail' => 'emails.user-activation', //activation email twig template
        'registerConfirmTemplate' => 'backend.auth.register-confirm', //activation confirm page

    //reset password
        'resetPasswordTemplate' => 'backend.auth.password', //reset password page twig template
        'resetPasswordSuccessTemplate' => 'backend.auth.reset-password-success', //reset password request success page twig template
        'resetPasswordEmailTemplate' => 'emails.user-reset-password',
        'resetPasswordLandingPageTemplate' => 'backend.auth.reset', //reset password confirm page twig
    ],
    // 'memberAuth' => [
    //     'loginTemplate' => 'views/auth/login.html',
    //     'redirectPath' => 'en/backend/locations/port-cluster/list',
    //     'loginPath' => 'en/backend/auth/login',
    //     'redirectAfterLogout' => 'en/backend/auth/login',
    // ],
];
