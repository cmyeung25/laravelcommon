<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationTable extends Migration {

	public function up() {
		Schema::create('notifications', function(Blueprint $table) {
			$table->increments('id');
			$table->string('type');
			$table->string('status')->nullable();
			$table->string('from')->nullable();
			$table->text('to')->nullable();
			$table->text('cc')->nullable();
			$table->text('bcc')->nullable();
			$table->string('subject')->nullable();
			$table->text('content')->nullable();
			$table->timestamps();
		});
	}

	public function down() {
		Schema::drop('notifications');
	}
}
