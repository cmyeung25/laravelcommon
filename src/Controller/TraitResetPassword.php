<?php

namespace Hbsz\LaravelCommon\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Config;
use Route;
use App;
use Password;
use Illuminate\Mail\Message;

trait TraitResetPassword
{
    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function getForgetPassword(Request $request)
    {
        return view(Config::get("login.{$this->getAuthGroup()}.resetPasswordTemplate"),[
            'actionUrl' => url($request->getPathInfo()),
        ]);
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postForgetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), ['email' => 'required|email']);

        if ($validator->fails()) {
            return static::failValidation($validator->getMessageBag());
        }

        $userModel = Config::get('auth.model');

        $response = Password::sendResetLink(['email' => $request->only('email')], function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        $path = $request->getPathInfo();
        $pathArr = explode('/',$path);
        array_splice($pathArr,-1);
        $pathArr[] = 'forget-password-sent';
        $path = implode($pathArr,'/');

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return static::successWithRedirect(url($path));
            case Password::INVALID_USER:
                return static::successWithRedirect(url($path));
                // return static::failValidation([
                //     'email' => [trans('page.forget_password.no_account_found')],
                // ]);
        }
    }

    public function getForgetPasswordSent(Request $request)
    {
      return view(Config::get("login.{$this->getAuthGroup()}.resetPasswordSuccessTemplate"),[]);
    }

    /**
     * Get the e-mail subject line to be used for the reset link email.
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : 'Your Password Reset Link';
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function getReset(Request $request)
    {
        $token = $request->get('token');
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        return view(Config::get("login.{$this->getAuthGroup()}.resetPasswordLandingPageTemplate"),[
            'reset_token' => $token,
            'actionUrl' => url($request->getPathInfo()),
        ]);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return static::failValidation($validator->getMessageBag());
        }
        $postData = $request->all();
        $credentials = [
            'email' => $postData['email'],
            'password' => $postData['password'],
            'password_confirmation' => $postData['password_confirmation'],
            'token' => $postData['token'],
        ];

        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return static::successWithRedirect(url('/'));
            case Password::INVALID_USER:
                return static::failValidation([
                    'email' => ['INVALID_USER'],
                ]);
            case Password::INVALID_PASSWORD:
                return static::failValidation([
                    'email' => ['INVALID_PASSWORD'],
                ]);
            case Password::INVALID_TOKEN:
                return static::failValidation([
                    'email' => ['INVALID_TOKEN'],
                ]);
            default:
                return static::failValidation([
                    'email' => [trans('page.forget_password.no_account_found')],
                ]);
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = bcrypt($password);

        $user->save();

        Auth::login($user);
    }
}
