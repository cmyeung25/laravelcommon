<?php

namespace Hbsz\LaravelCommon\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Validator;
use Config;
use DB;
use Mail;

trait TraitRegistersUsers
{
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister(Request $request)
    {
        return view(Config::get("login.{$this->getAuthGroup()}.registerTemplate"), [
            'actionUrl' => url($request->getPathInfo()),
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @todo Registration Validation with email token
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = Validator::make($request->all(), $this->getRegisterRules(), $this->getRegisterValidationMessage());

        if ($validator->fails()) {
            return static::failValidation($validator->getMessageBag());
        }

        $userModel = Config::get('auth.providers.users.model');
        $isDirectLogin = Config::get("login.{$this->getAuthGroup()}.registerDirectLogin");

        DB::beginTransaction();
        $user = new $userModel;
        $user->fill($this->createUserWhenRegister($request->all()));
        $user->save();
        // dd($user);

        // $user = $userModel::create($this->createUserWhenRegister($request->all()));

        $this->afterCreateUser($user, $request->all());

        if ($isDirectLogin) {
            Auth::login($user);
            $user->activated_at = new Carbon;
            $user->save();

            DB::commit();

            $redirectUrl = url(Config::get("login.{$this->getAuthGroup()}.redirectPath"));

            if($request->session()->has('login-redirect')){
                $redirectUrl = url($request->session()->pull('login-redirect'));
            }

            return static::successWithRedirect($redirectUrl);
        }

        $token = hash_hmac('sha256', str_random(40), Config::get('app.key'));
        $user->activation_code = $token;
        $user->save();
        Mail::send(Config::get("login.{$this->getAuthGroup()}.registerActivationEmail"), ['token' => $token], function ($m) use ($user) {
            $m->to($user->email);
            $m->subject('Activation Mail');
        });

        DB::commit();
    }

    /**
     * Allow user to confirm their registration after clcking the link inside the.
     */
    public function getConfirmRegistration(Request $request)
    {
        $token = $request->get('token');

        // $userModel = Config::get('auth.model');
        //
        // if( $userModel::where('activation_code', $token)->count() == 0 ) {
        //     die('Requested token is not exist!');
        // }

        return view(Config::get("login.{$this->getAuthGroup()}.registerConfirmTemplate"), [
            'token' => $token,
            'form' => [
                'action' => url($request->getPathInfo()),
            ],
        ]);
    }

    public function postConfirmRegistration(Request $request)
    {
        $postData = $request->all();

        $userModel = Config::get('auth.model');

        $user = $userModel::where('activation_code', $postData['token'])
            ->where('email', $postData['email'])->first();

        if (empty($user)) {
            return static::failValidation([
                'email' => ['We cannot find your record.'],
            ]);
        }

        $user->activated_at = DB::raw('now()');
        $user->save();
        Auth::login($user);

        return static::successWithRedirect(url(Config::get("login.{$this->getAuthGroup()}.redirectPath")));
    }

    protected function afterCreateUser($user, $postData) {

    }

    protected function getRegisterValidationMessage() {

    }

    abstract protected function createUserWhenRegister($postData);
    abstract protected function getRegisterRules();
    abstract protected function getAuthGroup();
}
