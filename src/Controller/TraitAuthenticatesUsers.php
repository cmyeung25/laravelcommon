<?php

namespace Hbsz\LaravelCommon\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Validator;
use Config;

trait TraitAuthenticatesUsers
{

    public function getIndex(Request $request)
    {
        return redirect($request->getPathInfo().'/login');
    }

    public function getLogin(Request $request)
    {
        $ref = $request->get('ref');
        if(!empty($ref)) {
          $request->session()->set('login-redirect',$ref);
        }

        $loginViewTemplate = Config::get("login.{$this->getAuthGroup()}.loginTemplate");
        if(empty($loginViewTemplate)) {
            die ("Missing view in {$this->getAuthGroup()}");
        }
        return view($loginViewTemplate, [
            "actionUrl" => url($request->getPathInfo())
        ]);
    }

    public function postLogin(Request $request)
    {
        $validator = Validator::make($request->all(), $this->getLoginRules());

        if($validator->fails()) {
            return static::failValidation($validator->getMessageBag());
        }
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::attempt($credentials, $request->has('remember')) &&
                $this->loginExtraValidation(Auth::user())
            ) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return static::failWithMessage($this->getFailedLoginMessage());
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            $this->authenticated($request, Auth::user());
        }

        $redirectUrl = url(Config::get("login.{$this->getAuthGroup()}.redirectPath"));

        if($request->session()->has('login-redirect')){
            $redirectUrl = url($request->session()->pull('login-redirect'));
        }
        return static::successWithRedirect($redirectUrl, 'Login Successful');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return Lang::has('auth.failed')
                ? Lang::get('auth.failed')
                : 'These credentials do not match our records.';
    }

    /**
     * Determine if the class is using the ThrottlesLogins trait.
     *
     * @return bool
     */
    protected function isUsingThrottlesLoginsTrait()
    {
        return in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );
    }

    protected function loginExtraValidation(AuthenticatableContract $user) {
        return true;
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        Auth::logout();

        return redirect(Config::get("login.{$this->getAuthGroup()}.redirectAfterLogout"));
    }


    protected abstract function getAuthGroup();
    protected abstract function getLoginRules();
}
